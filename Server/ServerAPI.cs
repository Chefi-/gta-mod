﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using CitizenFX.Core;
using static CitizenFX.Core.Native.API;
using static CitizenFX.Core.Entity;

namespace Server
{
    public class ServerAPI
    {
        public static void TriggerGlobalClientEvent(string name, params object[] args)
        {
            PlayerList players = new PlayerList();
            foreach (var player in players)
            {
                TriggerClientEvent(player, name, args);
            }
        }
        
        public static void TriggerClientEvent(Player player, string name, params object[] args)
        {
            string eventName = $"{Main.modName}:Client:{name}";
            if (name != "GMHeatChasedValues")
            {
                Debug.WriteLine($"Trigger event: {eventName}");
            }
            player.TriggerEvent(eventName, args);
        }

        public static void TriggerSubtitle(Player player, string notification, int duration = 2500)
        {
            player.TriggerEvent($"{Main.modName}:Client:Subtitle", notification, duration);
        }
        
        public static void TriggerGlobalSubtitle(string notification, int duration = 2500)
        {
            PlayerList players = new PlayerList();
            foreach (var player in players)
            {
                TriggerSubtitle(player, notification, duration);
            }
        }
        
        public static void TriggerGlobalCenterText(string notification, int duration = 2500, int alpha = 100, int style = 0)
        {
            PlayerList players = new PlayerList();
            foreach (var player in players)
            {
                player.TriggerEvent($"{Main.modName}:Client:CenterText", notification, duration, alpha, style);
            }
        }
        
        public static void TriggerGlobalNotification(string notification)
        {
            PlayerList players = new PlayerList();
            foreach (var player in players)
            {
                player.TriggerEvent($"{Main.modName}:Client:Notification", notification);
            }
        }
    }
}
