﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CitizenFX.Core;
using static CitizenFX.Core.Native.API;
using static CitizenFX.Core.Entity;

namespace Client
{
    public class ServerActions
    {
        public ServerActions() {}

        public static Player GetPlayer(int entity)
        {
            return new PlayerList()[entity];
        }
    }
}