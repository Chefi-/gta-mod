﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CitizenFX.Core;
using Newtonsoft.Json;
using static CitizenFX.Core.Native.API;

namespace Server
{
    public class LocationData
    {
        public float x;
        public float y;
        public float z;
        public float angle;
        public string createdBy;
    }
    
    public class Locations
    {
        public Locations() { }

        private List<LocationData> _locations;
        public List<LocationData> locations => _locations;

        public List<LocationData> LoadLocations()
        {
            string json = LoadResourceFile(GetCurrentResourceName(), "./locations.json");
            _locations = JsonConvert.DeserializeObject<List<LocationData>>(json);
            return _locations != null && _locations.Count > 0 ? _locations : new List<LocationData>();
        }

        public void SaveLocation(Vector3 point, float angle, string createdBy)
        {
            LocationData data = new LocationData();
            data.x = point.X;
            data.y = point.Y;
            data.z = point.Z;
            data.angle = angle;
            data.createdBy = createdBy;

            List<LocationData> locations = LoadLocations();
            
            // Append...
            locations.Add(data);
            
            Debug.WriteLine("locations len: " + locations.Count);
            
            using (FileStream fs = File.Open(@"Z:\GTA_FXServer\server-data\resources\NutsMod\locations.json", FileMode.OpenOrCreate))
            using (StreamWriter sw = new StreamWriter(fs))
            using (JsonWriter jw = new JsonTextWriter(sw))
            {
                jw.Formatting = Formatting.Indented;
 
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(jw, locations);
            }
        }

        public LocationData GetRandomLocation()
        {
            Random rand = new Random();

            if (_locations.Count <= 0)
            {
                Debug.WriteLine("No locations loaded...");
                return null;
            }

            int locationIndex = rand.Next(0, _locations.Count - 1);
            
            return _locations[locationIndex];
        }
    }
}