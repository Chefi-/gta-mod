﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CitizenFX.Core;
using Client;
using static CitizenFX.Core.Native.API;

namespace Server
{
    public class Main : BaseScript
    {
        public static readonly string modName = "NutsMod";

        private GameMode _gameMode;

        private Locations _locations;
        public Locations locations => _locations;
        
        public Main()
        {
            EventHandlers["chatMessage"] += new Action<int, string, string>(OnChatMessage);
            EventHandlers["onServerResourceStart"] += new Action<string>(OnServerResourceStart);
        }

        private void OnServerResourceStart(string resourceName)
        {
            if(GetCurrentResourceName() != resourceName) return;

            Debug.WriteLine($"The resource {resourceName} has been started.");
            
            RegisterCommand("carforall", new Action<int, List<object>, string>(OnCommandCarForAll), false);
            RegisterCommand("ready", new Action<int, List<object>, string>(OnCommandGMHeat), false);
            RegisterCommand("gmheat", new Action<int, List<object>, string>(OnCommandGMHeat), false);
            RegisterCommand("play", new Action(OnCommandGMHeatPause), false);
            RegisterCommand("gmplay", new Action(OnCommandGMHeatPause), false);
            RegisterCommand("pause", new Action(OnCommandGMHeatPause), false);
            RegisterCommand("gmpause", new Action(OnCommandGMHeatPause), false);
            RegisterCommand("skip", new Action(OnCommandGMHeatSkip), false);
            RegisterCommand("gmskip", new Action(OnCommandGMHeatSkip), false);
            RegisterCommand("gm_escape_distance", new Action<int, List<object>, string>(OnCommandGMHeatSetSetting), false);
            RegisterCommand("gm_catch_distance", new Action<int, List<object>, string>(OnCommandGMHeatSetSetting), false);
            RegisterCommand("gm_catch_time", new Action<int, List<object>, string>(OnCommandGMHeatSetSetting), false);
            RegisterCommand("log", new Action(OnServerLog), false);
            RegisterCommand("log2", new Action(OnServerLog2), false);
            RegisterCommand("log_players", new Action(OnLogPlayers), false);
            RegisterCommand("savelocation", new Action<int, List<object>, string>(OnSaveLocation), false);
            
            _locations = new Locations();
            _locations.LoadLocations();

            Tick += OnCheckUnsyncPlayers;
        }
        
        private Dictionary<string, float> _desyncTimer = new Dictionary<string, float>();

        private async Task OnCheckUnsyncPlayers()
        {
            await Delay(1000);
            
            var playerList = Players;
            foreach (var player in playerList)
            {
                if (player == null || player.Handle == null || player.Character == null) continue;
                
                if (!_desyncTimer.ContainsKey(player.Handle))
                {
                    _desyncTimer[player.Handle] = 0;
                }

                // if player entity stays zero health for too long, consider it as being desync
                // this has occurred few times and it ruins game mode as players coords and health is not correct
                if (GetEntityHealth(player.Character.Handle) <= 0)
                {
                    _desyncTimer[player.Handle] += 1000;
            
                    if (_desyncTimer[player.Handle] > 20000)
                    {
                        DropPlayer(player.Handle, "Please RESTART FiveM and rejoin (Reason: Desync)");
                    }
                }
                else
                {
                    _desyncTimer[player.Handle] = 0;
                }
            }
        }

        private int _escapeDistance;
        private int _catchDistance;
        private int _catchTime;

        private void OnLogPlayers()
        {
            Debug.WriteLine($"-- PLAYERS -----------------------");
            
            var playerList = new PlayerList();
            var i = 0;
            foreach (var player in playerList)
            {
                i++;
                Debug.WriteLine($"Player_{i}: {player.Name} [{player.Handle}]");
            }
        }

        private void OnServerLog()
        {
            ServerAPI.TriggerGlobalClientEvent("BecomeHelicopter");
        }
        
        private void OnServerLog2()
        {
            var playerList = new PlayerList();
            foreach (var player in playerList)
            {
                int ped = GetPlayerPed(player.Handle);
                int vehicle = GetVehiclePedIsIn(ped, true);
            }
        }

        private void OnCommandGMHeatSetSetting(int source, List<object> args, string raw)
        {
            string setting = raw.Split(' ')[0];
            string value = args.Count > 0 ? args[0].ToString() : "";
            
            GMHeat gm = (GMHeat) _gameMode;
            
            switch (setting)
            {
                case "gm_escape_distance":
                    if (value != "")
                    {
                        _escapeDistance = int.Parse(value);
                        gm?.SetEscapeDistance(_escapeDistance);
                    }
                    break;
                case "gm_catch_distance":
                    if (value != "")
                    {
                        _catchDistance = int.Parse(value);
                        gm?.SetCatchDistance(_catchDistance);
                    }
                    break;
                case "gm_catch_time":
                    if (value != "")
                    {
                        _catchTime = int.Parse(value);
                        gm?.SetCatchTime(_catchTime);
                    }
                    break;
            }
        }

        private void OnCommandCarForAll(int source, List<object> args, string raw)
        {
            string value = args.Count > 0 ? args[0].ToString() : "";
            ServerAPI.TriggerGlobalClientEvent("SpawnCar", value);
        }

        private async void OnCommandGMHeat(int source, List<object> args, string raw)
        {
            string value = args.Count > 0 ? args[0].ToString() : "";

            if (value == "end" || value == "stop")
            {
                EndGM();
            }
            else
            {
                string carModel = value;

                if (carModel != "")
                {
                    ServerAPI.TriggerGlobalClientEvent("SpawnCar", carModel);
                    await Delay(1000);
                }
                
                StartGM();
            }
        }

        private void OnCommandGMHeatPause()
        {
            if (_gameMode == null)
            {
                Debug.WriteLine("No game mode to pause"); 
                return;
            }
            _gameMode.TogglePause();
        }
        
        private void OnCommandGMHeatSkip()
        {
            if (_gameMode == null)
            {
                Debug.WriteLine("No game mode to skip round in"); 
                return;
            }
            _gameMode.SkipRound();
        }

        private void OnCommandGMHeatPlay()
        {
            if (_gameMode == null)
            {
                Debug.WriteLine("No game mode to unpause"); 
                return;
            }
            _gameMode.Pause(false);
        }

        public void EndGM()
        {
            if (_gameMode == null)
            {
                Debug.WriteLine($"No GM to end");
                return;
            }
            
            _gameMode.End();
            Tick -= _gameMode.OnTick;
        }

        private void StartGM()
        {
            if (_gameMode != null)
            {
                EndGM();
            }
            
            GMHeat gm = new GMHeat(this);
            if (_escapeDistance > 0) gm.SetEscapeDistance(_escapeDistance);
            if (_catchDistance > 0) gm.SetCatchDistance(_catchDistance);
            if (_catchTime > 0) gm.SetCatchTime(_catchTime);
            Tick += gm.OnTick;
            gm.Start();

            _gameMode = gm;
        }

        private void OnChatMessage(int source, string color, string message)
        {
            Player player = ServerActions.GetPlayer(source);

            string[] args = message.Split(' ');

            if (args[0] == "/info")
            {
                player.TriggerEvent($"{modName}:Client:Info");
                CancelEvent();
            }
        }
        
        private void OnSaveLocation(int source, List<object> args, string raw)
        {
            // Player player = new PlayerList()[source];

            Debug.WriteLine("Save location disabled...");
            // locations.SaveLocation(player.Character.Position, player.Character.Heading, player.Name);
        }
    }
}