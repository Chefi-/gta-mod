﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CitizenFX.Core;
using static CitizenFX.Core.Native.API;
#pragma warning disable 1998

namespace Server
{
    public class GMHeat : GameMode
    {
        private static bool DEV_MODE = false;
        
        private static bool HELICOPTER_ENABLED = true;
        
        private long _prevTickTime;
        private long _deltaTime;
        
        private Vector3 _roundStartPoint = new Vector3(573.0f, -3016.0f, 6.0f);
        private int _roundStartHeading;
        private bool _roundStartRandomHeading;

        private Player _chasedPlayer;
        
        private int _roundStartTime = DEV_MODE ? 1000 : 1000 * 15;
        // private int _roundStartTime = DEV_MODE ? 1000 : 1000 * 10;
        // private int _roundStartTime = DEV_MODE ? 1000 : 1000 * 3;

        private int _countdownTime = DEV_MODE ? 1000 : 5000;
        
        private int _turnsPlayed;
        private int _roundsPlayed;

        private Dictionary<Player, int> _turnsEscapedByPlayer = new Dictionary<Player, int>();

        private int _chaseEscapeDistance = 200;
        
        private int _helicopterSpawnDistance = 300;

        private Team _winnerTeam = Team.None;
        private Player _winnerChaser;
        private bool _chasedDead;

        private bool _helicopterSpawned;

        private int _difficultyLevel;

        private long _chaseStartTime;

        private int _maxHeat = 100;
        
        private int _heatGainSpeedMax = 20;
        
        private int _heatFadeSpeedMax = 20;
        
        private int _heatGainMinDistance = 6;
        
        private int _heatGainInVehicleMaxDistance = 15;
        private int _heatGainOnFootMaxDistance = 7;

        private float _currentHeatGainPercentage;
        
        private float _currentHeat;
        
        private float _currentHeatPercent;

        private int _catchProtectionTime = 2000;

        private int _chasedSpeedLimiter = 99; // +150km/h
        // private int _chasedSpeedLimiter = 42; // +150km/h
        // private int _chasedSpeedLimiter = 27; // +150km/h
        
        private List<Vector2> _spawnGrid;

        private Scores _scores;

        enum State
        {
            None,
            RoundStarting,
            CountDown,
            PreChase,
            Chase,
            PostChase,
        }
        
        enum Team
        {
            None,
            Chaser,
            Chased,
        }

        private State _state = State.None;

        public GMHeat(Main main):base(main)
        {
            _name = "Heat";
            
            float spawnGridXStep = 4.0f;
            float spawnGridYStep = 7.0f;
            float spawnSideOffsetY = -2.0f;
            
            _spawnGrid = new List<Vector2>();
            _spawnGrid.Add(new Vector2(0.0f, 0.0f)); // chased player
            _spawnGrid.Add(new Vector2(-spawnGridXStep, spawnSideOffsetY));
            _spawnGrid.Add(new Vector2(spawnGridXStep, spawnSideOffsetY));
            
            _spawnGrid.Add(new Vector2(0.0f, -spawnGridYStep));
            _spawnGrid.Add(new Vector2(-spawnGridXStep, -spawnGridYStep + spawnSideOffsetY));
            _spawnGrid.Add(new Vector2(spawnGridXStep, -spawnGridYStep + spawnSideOffsetY));
            
            _spawnGrid.Add(new Vector2(0.0f, -spawnGridYStep * 2));
            _spawnGrid.Add(new Vector2(-spawnGridXStep, -spawnGridYStep * 2 + spawnSideOffsetY));
            _spawnGrid.Add(new Vector2(spawnGridXStep, -spawnGridYStep * 2 + spawnSideOffsetY));
        }

        public override async void Start()
        {
            _prevTickTime = 0;

            base.Start();
            
            _scores = new Scores();

            SetState(State.RoundStarting);
        }

        private Vector3 GetPlayerStartPoint(int spawnGridIndex, int spawnAngle)
        {
            Vector2 spawnOffset = _spawnGrid[spawnGridIndex];
            Vector2 spawnPointCenter2d = new Vector2(_roundStartPoint.X, _roundStartPoint.Y);
            Vector2 spawnPoint2d = spawnPointCenter2d + MathUtils.RotateAroundPoint(spawnOffset, Vector2.Zero, spawnAngle);

            float zOffset = _roundStartPoint.Z + 4.0f;

            return new Vector3(spawnPoint2d.X, spawnPoint2d.Y, zOffset);
        }

        private List<Player> GetChasingPlayers()
        {
            PlayerList playerList = new PlayerList();

            List<Player> players = new List<Player>();
            
            foreach (var player in playerList)
            {
                if (player == _chasedPlayer) continue; 
                players.Add(player);
            }

            return players;
        }

        private void GetNearestPlayer(List<Player> players, Player target, out Player nearestPlayer, out Vector3 nearestPosition, out float nearestDistance, out float nearestDistance2d)
        {
            nearestPlayer = null;
            nearestPosition = _roundStartPoint;
            nearestDistance = float.MaxValue;
            nearestDistance2d = float.MaxValue;

            if (_chasedPlayer == null) return;
            
            // default to round start point for testing purposes etc.
            nearestDistance = Vector3.Distance(_roundStartPoint, _chasedPlayer.Character.Position);
            nearestDistance2d = nearestDistance;

            if (players.Count > 0)
            {
                nearestDistance = float.MaxValue;
            
                foreach (Player player in players)
                {
                    if (player == target) continue;

                    float dist = Vector3.Distance(_chasedPlayer.Character.Position, player.Character.Position);
                    
                    float dist2d = Vector3.Distance(
                        new Vector3(_chasedPlayer.Character.Position.X, _chasedPlayer.Character.Position.Y, 0.0f), 
                        new Vector3(player.Character.Position.X, player.Character.Position.Y, 0.0f)
                    );
                    
                    if (dist < nearestDistance)
                    {
                        nearestPlayer = player;
                        nearestPosition = player.Character.Position;
                        nearestDistance = dist;
                    }

                    if (dist2d < nearestDistance2d)
                    {
                        nearestDistance2d = dist2d;
                    }
                }
            }
        }

        private void SetState(State state)
        {
            Debug.WriteLine($"State change: {_state} -> {state}");

            _state = state;

            if (_state != State.None)
            {
                switch (_state)
                {
                    case State.Chase:
                        StateChaseEnter();
                        break;
                }
            }
        }

        private void SetNextStartPoint()
        {
            LocationData locationData = _main.locations.GetRandomLocation();
            _roundStartPoint = new Vector3(locationData.x, locationData.y, locationData.z);
            
            // if more than 3 players, lock the spawn heading angle, as more space required
            _roundStartHeading = (int) locationData.angle;
            _roundStartRandomHeading = new PlayerList().ToList().Count > 3;
        }
        
        private Player GetNextChasedPlayer()
        {
            List<Player> players = new PlayerList().ToList();

            if (players.Count <= 0) return null;
            
            Player nextPlayer = players
                .SkipWhile(player => player != _chasedPlayer)
                .Skip(1)
                .FirstOrDefault();
            
            return nextPlayer ?? players.First();
        }
        
        public override async Task OnTick()
        {
            _deltaTime = _prevTickTime > 0
                ? GetGameTimer() - _prevTickTime
                : 0;

            _prevTickTime = GetGameTimer();
            
            if (_pause) return;

            switch (_state)
            {
                case State.RoundStarting:
                    await StateRoundStartingTick();
                    break;
                
                case State.CountDown:
                    await StateCountDownTick();
                    break;
                
                case State.PreChase:
                    await StatePreChaseTick();
                    break;
                
                case State.Chase:
                    await StateChaseTick();
                    break;
                
                case State.PostChase:
                    await StatePostChaseTick();
                    break;
            }
        }

        private async Task StateRoundStartingTick()
        {
            _turnsPlayed++;
            _difficultyLevel = 0;
            _chasedDead = false;
            _helicopterSpawned = false;
            _chaseTime = -1;

            ResetHeat();

            int startTime = _turnsPlayed <= 1 ? 5000 : _roundStartTime;

            bool isNewRound = _chasedPlayer == Players.ToList().LastOrDefault();
            if (isNewRound)
            {
                // new round...
                _roundsPlayed++;
            }

            _chasedPlayer = GetNextChasedPlayer();
            
            if (_chasedPlayer == null)
            {
                Debug.WriteLine("End game mode: No next chased player found...");
                _main.EndGM();
                return;
            }
            
            int chasedHandle = _chasedPlayer != null ? int.Parse(_chasedPlayer.Handle) : -1;
            ServerAPI.TriggerGlobalClientEvent("GMHeatChasedValues", chasedHandle, -1, -1, -1, -1);
            ServerAPI.TriggerGlobalClientEvent("SetDifficultyLevel", _difficultyLevel);

            await Delay(500);
            
            ServerAPI.TriggerGlobalCenterText($"NEXT: {_chasedPlayer.Name}", startTime, 200);
            ServerAPI.TriggerGlobalClientEvent("GMRoundStarting");

            float timer = startTime;
            while (timer > 0.0f)
            {
                PlayerList playerList = new PlayerList();
                foreach (var player in playerList)
                {
                    if (player == _chasedPlayer)
                    {
                        ServerAPI.TriggerSubtitle(player, $"~b~YOU ARE NEXT!~s~ FIND NEW CAR ({Math.Ceiling(timer / 1000)})");
                    }
                    else
                    {
                        ServerAPI.TriggerSubtitle(player, $"~y~Like this car?~s~ Prevent ~b~{_chasedPlayer.Name}~s~ from changing car. ({Math.Ceiling(timer / 1000)})");
                    }
                }
                
                await Delay(1000);
                
                timer -= 1000;
            }
            
            if (isNewRound)
            {
                _scores.ResetCurrentRoundScores();
                UpdateClientScores();
            }
            
            SetState(State.CountDown);
        }

        private async Task StateCountDownTick()
        {
            while (_pause) await Delay(0);
            bool spawnSuccess = false;
            int maxRetryAttempts = 2;
            int retryAttempts = 0;
            while (spawnSuccess == false && retryAttempts <= maxRetryAttempts)
            {
                ServerAPI.TriggerGlobalNotification($"Chase ~b~{_chasedPlayer.Name}~s~");
                
                SetNextStartPoint();
                
                Random rand = new Random();
                int spawnAngle = _roundStartRandomHeading ? rand.Next(0, 359) : _roundStartHeading;

                SpawnPlayers(spawnAngle);

                float timer = _countdownTime;
                while (timer > 0.0f)
                {
                    while (_pause) await Delay(0);
                
                    ServerAPI.TriggerGlobalCenterText($"{Math.Ceiling(timer / 1000)}", 1100, 150, 1);
                    await Delay(1000);
                
                    timer -= 1000;
                }
            
                // Check if all players near each other. If not, retry spawn.
                spawnSuccess = AllPlayersInSpawn();

                if (!spawnSuccess)
                {
                    retryAttempts++;
                    
                    Debug.WriteLine($"Spawn failed: Not all players in spawn. Retry spawn ({retryAttempts})");
                    ServerAPI.TriggerGlobalSubtitle($"~r~Spawn failed: Not all players in spawn. Retry spawn ({retryAttempts})~s~");
                }
            }
            
            ServerAPI.TriggerGlobalSubtitle($"");

            SetState(State.PreChase);
        }

        private bool AllPlayersInSpawn()
        {
            float maxDistance = float.MinValue;
            
            PlayerList playerList = new PlayerList();
            foreach (Player playerA in playerList)
            {
                foreach (Player playerB in playerList)
                {
                    float distance = Vector3.Distance(playerA.Character.Position, playerB.Character.Position);
                    maxDistance = Math.Max(distance, maxDistance);
                }
            }

            // check that all players relatively close to each other
            return maxDistance < 15.0f;
        }

        private async void SpawnPlayers(int spawnAngle)
        {
            // First spawn chased player
            Vector3 spawnPoint = GetPlayerStartPoint(0, spawnAngle);
            
            ServerAPI.TriggerClientEvent(_chasedPlayer, "GMSpawn", spawnPoint, spawnAngle, false);
            
            ServerAPI.TriggerGlobalClientEvent( "DisableControl");
            ServerAPI.TriggerGlobalClientEvent( "ResetCar");

            await Delay(2000);

            var spawnGridIndex = 1; // 0 reserved for chased player
            PlayerList playerList = new PlayerList();
            foreach (Player player in playerList)
            {
                if (player == _chasedPlayer) continue;

                spawnPoint = GetPlayerStartPoint(spawnGridIndex, spawnAngle);
                spawnGridIndex++;
                
                ServerAPI.TriggerClientEvent(player, "GMSpawn", spawnPoint, spawnAngle, true);
            }
        }

        private async Task StatePreChaseTick()
        {
            ServerAPI.TriggerClientEvent(_chasedPlayer, "EnableControl");
            ServerAPI.TriggerGlobalClientEvent( "SetSpeedLimiter", 200);
            await Delay(500);
            ServerAPI.TriggerClientEvent(_chasedPlayer, "SetSpeedLimiter", _chasedSpeedLimiter);
            
            await Delay(1000);
            
            SetState(State.Chase);
        }

        private void StateChaseEnter()
        {
            _chaseStartTime = GetGameTimer();

            PlayerList playerList = new PlayerList();
            foreach (Player player in playerList)
            {
                if (player != _chasedPlayer)
                {
                    ServerAPI.TriggerClientEvent(player, "SetSpeedLimiter", 99);
                    ServerAPI.TriggerClientEvent(player, "EnableControl");
                }
            }
        }

        private long _chaseTime;
        
        private async Task StateChaseTick()
        {
            if (_skipRound)
            {
                _skipRound = false;
                ServerAPI.TriggerGlobalNotification("ROUND SKIPPED");
                Pause(true);
                
                SetState(State.PostChase);
                return;
            } 
            
            _chaseTime = GetGameTimer() - _chaseStartTime;

            // Check if chased player is missing
            if (_chasedPlayer == null || _chasedPlayer.Character == null)
            {
                ServerAPI.TriggerGlobalSubtitle($"Chased player disappeared?!");

                _winnerTeam = Team.Chaser;
                    
                SetState(State.PostChase);
                return;
            }

            List<Player> chasingPlayers = GetChasingPlayers();
            GetNearestPlayer(
                chasingPlayers, 
                _chasedPlayer,
                out Player nearestChaser, 
                out Vector3 nearestPosition, 
                out float nearestDistance, // used to calculate heat (we don't want helicopter to be able to catch from air, so count Z axis to heat distance)
                out float nearestDistance2d // used to calculate escape (we don't want Z axis or falling off a mountain or helicopter flying high to affect escape)
            );
            
            // check if chased player is dead
            if (GetEntityHealth(_chasedPlayer.Character.Handle) <= 0)
            {
                _winnerTeam = Team.Chaser;
                _chasedDead = true;
                    
                SetState(State.PostChase);
                return;
            }

            if (nearestDistance2d > _chaseEscapeDistance)
            {
                _winnerTeam = Team.Chased;
                
                SetState(State.PostChase);
                return;
            }
            
            if (_catchProtectionTime < _chaseTime)
            {
                CalculateHeat(nearestDistance);

                ServerAPI.TriggerGlobalClientEvent(
                    "GMHeatChasedValues", 
                    int.Parse(_chasedPlayer.Handle), 
                    (int)nearestDistance, 
                    Math.Round(_currentHeatPercent * 100), 
                    Math.Round(_currentHeatGainPercentage * 100),
                    (int)_chaseTime
                    );

                if (_currentHeatPercent >= 1)
                {
                    _winnerTeam = Team.Chaser;
                    _winnerChaser = nearestChaser;
                        
                    SetState(State.PostChase);
                    return;
                }
            }

            // increase difficulty level every minute
            int difficultyLevel = (int)Math.Floor(_chaseTime / 60000.0f);

            if (difficultyLevel != _difficultyLevel)
            {
                _difficultyLevel = difficultyLevel;
                ServerAPI.TriggerGlobalClientEvent("SetDifficultyLevel", _difficultyLevel);
            }

            // Check if we have player that has been left behind and could become helicopter
            if (chasingPlayers.Count > 1 && !_helicopterSpawned && HELICOPTER_ENABLED)
            {
                foreach (var player in chasingPlayers)
                {
                    float distanceToChased = Vector3.Distance(player.Character.Position, _chasedPlayer.Character.Position);
                    if (distanceToChased > _helicopterSpawnDistance)
                    {
                        _helicopterSpawned = true;
                        ServerAPI.TriggerClientEvent(player, "BecomeHelicopter", _chasedPlayer.Character.Position, _chasedPlayer.Character.Heading);
                    }
                }
            }

            ChaseMessage(nearestChaser);
        }

        private float _chaseMessageLastUpdated;

        private void ChaseMessage(Player nearestChaser)
        {
            if (GetGameTimer() - _chaseMessageLastUpdated < 2000) return;

            _chaseMessageLastUpdated = GetGameTimer();
            
            PlayerList playerList = new PlayerList();
            foreach (var player in playerList)
            {
                if (player == _chasedPlayer)
                {
                    if (nearestChaser != null)
                    {
                        ServerAPI.TriggerSubtitle(player, $"Nearest to you ~b~{nearestChaser.Name}~s~ ~y~Get away!~s~", 2100);
                    }
                    else
                    {
                        ServerAPI.TriggerSubtitle(player, $"~y~Get away!~s~", 2100);
                    }
                }
                else
                {
                    ServerAPI.TriggerSubtitle(player, $"~y~Catch~s~ ~b~{_chasedPlayer.Name}!", 2100);
                }
            }
        }

        private async Task StatePostChaseTick()
        {
            if (_winnerTeam == Team.Chased)
            {
                string name = _chasedPlayer != null ? _chasedPlayer.Name : "UNKNOWN";
                ServerAPI.TriggerGlobalSubtitle($"~b~{name}~s~ escaped", 7000);
                ServerAPI.TriggerGlobalNotification($"{name} escaped");
                
                if (_chasedPlayer != null)
                {
                    _turnsEscapedByPlayer[_chasedPlayer] = _turnsEscapedByPlayer.ContainsKey(_chasedPlayer)
                        ? _turnsEscapedByPlayer[_chasedPlayer] + 1
                        : 1;
                }
                
                if (_chasedPlayer != null)
                {
                    ServerAPI.TriggerClientEvent(_chasedPlayer, "GMHeatEscapedChased");
                }
                
                var chasingPlayers = GetChasingPlayers();
                foreach (var player in chasingPlayers)
                {
                    ServerAPI.TriggerClientEvent(player, "GMHeatEscapedChasers");
                }
            }
            else if (_winnerTeam == Team.Chaser)
            {
                string name = _chasedPlayer != null ? _chasedPlayer.Name : "UNKNOWN";
                
                if (_chasedDead)
                {
                    ServerAPI.TriggerGlobalSubtitle($"{name} died brutally and alone :'(", 7000);
                }
                else
                {
                    string chaserName = _winnerChaser != null ? _winnerChaser.Name : "UNKNOWN";
                    ServerAPI.TriggerGlobalSubtitle($"{name} got caught by {chaserName}", 7000);
                }
                
                if (_chasedPlayer != null)
                {
                    ServerAPI.TriggerClientEvent(_chasedPlayer, "GMHeatCaughtChased");
                }
                
                var chasingPlayers = GetChasingPlayers();
                foreach (var player in chasingPlayers)
                {
                    ServerAPI.TriggerClientEvent(player, "GMHeatCaughtChasers");
                }
            }

            var players = Players.ToList();
            if (players.Count > 0)
            {
                bool roundEnd = _chasedPlayer == players.Last();
                UpdateScores(roundEnd);
                UpdateClientScores();
            }

            await Delay(5000);
            
            SetState(State.RoundStarting);
        }

        private void UpdateScores(bool roundEnd = false)
        {
            var scoresData = _scores.scoresData ?? new Scores.ScoresData();

            scoresData.data = scoresData.data ?? new List<Scores.ScoreData>();
            scoresData.roundsPlayed = _roundsPlayed;

            PlayerList playerList = new PlayerList();
            foreach (var player in playerList)
            {
                Scores.ScoreData scoreData = scoresData.data.Find(data => data.playerHandle == player.Handle);
                if (scoreData == null)
                {
                    scoreData = new Scores.ScoreData();
                    scoreData.playerHandle = player.Handle;
                    scoresData.data.Add(scoreData);
                }
                
                scoreData.turnsEscaped = _turnsEscapedByPlayer.ContainsKey(player)
                    ? _turnsEscapedByPlayer[player]
                    : 0;
                
                if (_chasedPlayer == player)
                {
                    if (_winnerTeam == Team.Chased)
                    {
                        scoreData.bestEscapeTime = _chaseTime < scoreData.bestEscapeTime || _chaseTime == -1
                            ? scoreData.bestEscapeTime
                            : (int)_chaseTime;
                        scoreData.currentRoundEscaped = true;
                    }
                    else
                    {
                        scoreData.currentRoundEscaped = false;
                    }
                    
                    scoreData.currentRoundTime = (int)_chaseTime;
                }
            }

            if (scoresData.data != null && scoresData.data.Count > 0)
            {
                // order, best first
                scoresData.data = scoresData.data
                    .OrderBy(d => !d.currentRoundEscaped)
                    .ThenBy(d => d.currentRoundTime)
                    .ToList();

                Scores.ScoreData firstData = scoresData.data.First();
                bool isBestRound = false;
                foreach (var player in playerList)
                {
                    if (firstData.playerHandle != player.Handle) continue;

                    if (scoresData.bestTurnEscaped && !firstData.currentRoundEscaped) break;

                    if (firstData.currentRoundEscaped && !scoresData.bestTurnEscaped)
                    {
                        isBestRound = true;
                        break;
                    }

                    if (!firstData.currentRoundEscaped && !scoresData.bestTurnEscaped)
                    {
                        isBestRound = firstData.currentRoundTime > scoresData.bestTurnTime;
                        break;
                    }

                    isBestRound = firstData.currentRoundTime < scoresData.bestTurnTime;
                }

                if (isBestRound)
                {
                    scoresData.bestTurnBy = firstData.playerHandle;
                    scoresData.bestTurnEscaped = firstData.currentRoundEscaped;
                    scoresData.bestTurnTime = firstData.currentRoundTime;
                }
            }

            if (roundEnd)
            {
                int scoreToAdd = 10;
            
                foreach (var scoreData in scoresData.data)
                {
                    scoreData.totalScore += scoreToAdd;
                    scoreToAdd--;
            
                    if (scoreToAdd <= 0) break;
                }
            }
            
            _scores.SetScores(scoresData);
        }

        private void UpdateClientScores()
        {
            ServerAPI.TriggerGlobalClientEvent("UpdateScores", _scores.ScoresToJSONString(_scores.scoresData));
        }

        private void ResetHeat()
        {
            _currentHeat = 0;
            _currentHeatPercent = 0;
            _currentHeatGainPercentage = 0;
        }
        
        private void CalculateHeat(float nearestDistance)
        {
            int vehicle = GetVehiclePedIsIn(_chasedPlayer.Character.Handle, false);
            float heatDistance = vehicle <= 0 ? _heatGainOnFootMaxDistance : _heatGainInVehicleMaxDistance;
            
            if (nearestDistance < heatDistance)
            {
                float gainPercent = 1 - Math.Max(0, nearestDistance - _heatGainMinDistance) / (heatDistance - _heatGainMinDistance);
                
                // apply easing, so that being close to chased accelerates heat gain a lot and gets more rewarding
                _currentHeatGainPercentage = Utils.EaseInQuad(gainPercent, 0.0f, gainPercent, 1);
                _currentHeat += (_currentHeatGainPercentage * _heatGainSpeedMax) * (_deltaTime / 1000.0f);
                _currentHeat = Math.Min(_currentHeat, _maxHeat);
                
                _currentHeatPercent = _currentHeat / _maxHeat;
            }
            else if (_currentHeat > 0)
            {
                _currentHeat -= _heatFadeSpeedMax * (_deltaTime / 1000.0f);
                _currentHeat = Math.Max(0, _currentHeat);

                _currentHeatPercent = _currentHeat / _maxHeat;
            }
        }

        public override void End()
        {
            base.End();

            _roundsPlayed = 0;
            _scores.SetScores(new Scores.ScoresData());
            UpdateClientScores();

            PlayerList playerList = new PlayerList();
            foreach (Player player in playerList)
            {
                ServerAPI.TriggerClientEvent(player, "EnableControl");
                ServerAPI.TriggerClientEvent(player, "SetSpeedLimiter", 99);
            }
        }

        public void SetEscapeDistance(int distance)
        {
            _chaseEscapeDistance = distance;
        }
        
        public void SetCatchDistance(int distance)
        {
            // _chaseCatchDistance = distance;
        }
        
        public void SetCatchTime(int time)
        {
            // _catchTime = time;
        }
    }
}