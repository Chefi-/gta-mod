﻿using System;

namespace Server
{
    public class Utils
    {
        public static float EaseInQuad(float time, float begin, float change, float duration)
        {
            time /= duration;
            return Math.Max(0, Math.Min(1, change * time * time + begin));
        }
    }
}