﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using CitizenFX.Core;
using static CitizenFX.Core.Native.API;

namespace Server
{
    public class GameMode : BaseScript
    {
        protected string _name;
        protected Main _main;
        protected bool _pause;
        protected bool _skipRound;

        public GameMode(Main main)
        {
            _main = main;
        }

#pragma warning disable 1998
        public virtual async void Start()
#pragma warning restore 1998
        {
            Debug.WriteLine($"Start game mode {_name}");
        }
        
        public virtual void End()
        {
            Debug.WriteLine($"End game mode {_name}");
        }

#pragma warning disable 1998
        public virtual async Task OnTick()
#pragma warning restore 1998
        {
            
        }

        public string GetName()
        {
            return _name;
        }

        public void Pause(bool pause)
        {
            _pause = pause;
            
            ServerAPI.TriggerGlobalNotification(_pause ? "GAME PAUSED" : "GAME UNPAUSED");
        }
        
        public void TogglePause()
        {
           Pause(!_pause);
        }

        public void SkipRound()
        {
            // unpause to let skip round kick in
            // skip round should repause automatically
            _pause = false;
            _skipRound = true;
        }
    }
}