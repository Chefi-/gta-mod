﻿using System;
using System.Drawing;

namespace Client
{
    public class Utils
    {
        public static float Lerp(float a, float b, float t)
        {
            return a * (1 - t) + b * t;
        }
        
        public static Color LerpColor(Color a, Color b, float t)
        {
            return Color.FromArgb(
                (int)Math.Round(Lerp(a.R, b.R, t)),
                (int)Math.Round(Lerp(a.G, b.G, t)),
                (int)Math.Round(Lerp(a.B, b.B, t))
            );
        }
    }
}