﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Client
{
    public class Scores
    {
        private ScoresData _scoresData;
        public ScoresData scoresData => _scoresData;
        
        public class ScoreData
        {
            public string playerHandle;
            public int turnsEscaped = -1;
            public int bestEscapeTime = -1;
            
            public bool currentRoundEscaped;
            public int currentRoundTime = -1;

            public int totalScore = 0;
        }
        
        public class ScoresData
        {
            public int roundsPlayed;
            public string bestTurnBy;
            public bool bestTurnEscaped;
            public int bestTurnTime;
            public List<ScoreData> data;
        }

        public Scores() { }

        public void SetScores(ScoresData scoresData)
        {
            _scoresData = scoresData;
        }
        
        public string ScoresToJSONString(ScoresData data)
        {
            return JsonConvert.SerializeObject(data);
        }
        
        public ScoresData ScoresJSONTOObject(string data)
        {
            return JsonConvert.DeserializeObject<ScoresData>(data);
        }
    }
}