﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.UI;
using static CitizenFX.Core.Native.API;
#pragma warning disable 1998

namespace Client
{
    public class Main : BaseScript
    {
        private static readonly string MOD_NAME = "NutsMod";

        private int _preHelicopterVehicleHandle;
        private int _preHelicopterVehicleModel;
        private Vehicle _helicopter;
        private int _helicopterAttachCooldownTime = 5000;
        private int _helicopterRespawnTime = 15000;
        private List<Prop> _helicopterBombs = new List<Prop>();
        
        private Color _heatColorMax = Color.FromArgb(255, 0, 0);
        private Color _heatColorMin = Color.FromArgb(255, 255, 0);
        private Color _fadeColor = Color.FromArgb(0, 255, 255);

        private Scores _scores;
        private Vehicles _vehicles;
        
        private float _heatPercent = -1;
        private float _heatAppliedCurrently = -1;
        private int _heatDistance = -1;
        private int _chasedPlayerHandle = -1;
        private int _chaseTime = -1;

        private string _centerText;
        private Color _centerTextColor;
        private float _centerTextTimer;
        private int _centerTextAlpha;
        private int _centerTextStyle;

        private int _wantedLevel;
        private int _difficultyLevel;
        private string _difficultyDescription;

        public Main()
        {
            EventHandlers["onClientResourceStart"] += new Action<string>(OnClientResourceStart);
            EventHandlers["onClientResourceStop"] += new Action<string>(OnClientResourceStop);
        }

        private void OnClientResourceStart(string resourceName)
        {
            if(GetCurrentResourceName() != resourceName) return;

            Debug.WriteLine($"The resource {resourceName} has been started.");
            
            EventHandlers[$"{MOD_NAME}:Client:Info"] += new Action(OnInfo);
            EventHandlers[$"{MOD_NAME}:Client:ResetCar"] += new Action(OnResetCar);
            EventHandlers[$"{MOD_NAME}:Client:DisableControl"] += new Action(OnDisableControl);
            EventHandlers[$"{MOD_NAME}:Client:EnableControl"] += new Action(OnEnableControl);
            EventHandlers[$"{MOD_NAME}:Client:Subtitle"] += new Action<string, int>(OnServerCommandSubtitle);
            EventHandlers[$"{MOD_NAME}:Client:Notification"] += new Action<string>(OnServerCommandNotification);
            EventHandlers[$"{MOD_NAME}:Client:CenterText"] += new Action<string, int, int, int>(OnServerCommandCenterText);
            EventHandlers[$"{MOD_NAME}:Client:GMSpawn"] += new Action<Vector3, int, bool>(OnServerCommandGMSpawn);
            EventHandlers[$"{MOD_NAME}:Client:GMRoundStarting"] += new Action(OnServerCommandGMRoundStarting);
            EventHandlers[$"{MOD_NAME}:Client:GMHeatChasedValues"] += new Action<int, int, int, int, int>(OnServerCommandGMHeatChasedValues);
            EventHandlers[$"{MOD_NAME}:Client:GMHeatCaughtChased"] += new Action(OnServerCommandGMHeatCaughtChased);
            EventHandlers[$"{MOD_NAME}:Client:GMHeatCaughtChasers"] += new Action(OnServerCommandGMHeatCaughtChasers);
            EventHandlers[$"{MOD_NAME}:Client:GMHeatEscapedChased"] += new Action(OnServerCommandGMHeatEscapedChased);
            EventHandlers[$"{MOD_NAME}:Client:GMHeatEscapedChasers"] += new Action(OnServerCommandGMHeatEscapedChasers);
            EventHandlers[$"{MOD_NAME}:Client:SpawnCar"] += new Action<string>(OnServerCommandSpawnCar);
            EventHandlers[$"{MOD_NAME}:Client:SetSpeedLimiter"] += new Action<int>(OnServerCommandSetSpeedLimiter);
            EventHandlers[$"{MOD_NAME}:Client:SetDifficultyLevel"] += new Action<int>(OnServerCommandSetDifficultyLevel);
            EventHandlers[$"{MOD_NAME}:Client:BecomeHelicopter"] += new Action<Vector3, int>(OnServerCommandBecomeHelicopter);
            EventHandlers[$"{MOD_NAME}:Client:UpdateScores"] += new Action<string>(OnServerCommandUpdateScores);
            EventHandlers[$"{MOD_NAME}:Client:Kill"] += new Action(OnServerCommandKill);

            RegisterCommand("car", new Action<int, List<object>, string>(OnCommandSpawnCar), false);
            RegisterCommand("fix", new Action<int, List<object>, string>(OnCommandFixCar), false);
            RegisterCommand("passive", new Action<int, List<object>, string>(OnCommandPassiveMode), false);
            RegisterCommand("suicide", new Action(OnCommandSuicide), false);
            RegisterCommand("clientlog", new Action<int, List<object>, string>(OnClientLog), false);
            RegisterCommand("clientlog2", new Action(OnClientLog2), false);
            RegisterCommand("cleararea", new Action(OnClearArea), false);
            RegisterCommand("test_helicopter", new Action(OnCommandTestHelicopter), false);

            Tick += OnUpdateNewPlayerBlipsTick;
            Tick += OnTick;
            Tick += OnDifficultyLevelTick;
            Tick += OnRespawnTick;
            Tick += OnHelicopterAttackTick;
            Tick += OnHelicopterBombsTick;
            Tick += OnHelicopterTick;
            Tick += OnDrawScores;
            Tick += OnSpeedLimiterTick;
            Tick += OnUpdatePlayerBlips;
            
            DoScreenFadeOut(0);
            DoScreenFadeIn(500);
            
            NetworkSetFriendlyFireOption(true);
            SetCanAttackFriendly(Game.PlayerPed.Handle, true, true);
            
            OnEnableControl();

            _difficultyLevel = -1;
            _difficultyDescription = "";
            
            SetPlayerWantedLevelNoDrop(Game.Player.Handle, 0, false);
            SetPlayerWantedLevelNow(Game.Player.Handle, false);
            
            _scores = new Scores();
            
            _vehicles = new Vehicles();
            _vehicles.LoadVehiclesData();

            SetTestScores();
        }

        private async Task OnSpeedLimiterTick()
        {
            int vehicle = GetVehiclePedIsIn(Game.PlayerPed.Handle, false);
            if (vehicle <= 0) return;
            
            // TODO: Overheat engine if going full speed for long?

            if (GetChasedPlayer() == Game.Player)
            {
                SetVehicleCheatPowerIncrease(vehicle, 0.9f); // nerf the chased power 10%
            }
            else
            {
                SetVehicleCheatPowerIncrease(vehicle, 1.0f);
            }
        }
        
        private void OnCommandTestHelicopter()
        {
            Vector3 coords = GetEntityCoords(Game.PlayerPed.Handle, true);
            BecomeChaserHelicopter(coords, 0);
        }

        private bool CanRespawnAsHelicopter()
        {
            return _helicopter != null && GetChasedPlayer() != null;
        }

        private async Task OnHelicopterTick()
        {
            if (!IsPedVehicleBuzzard(true)) return;

            var vehicle = GetVehiclePedIsIn(Game.PlayerPed.Handle, true);
            
            // check if we need to respawn helicopter chaser
            if (CanRespawnAsHelicopter() && (GetEntityHealth(vehicle) <= 0 || !IsPedInVehicle(Game.PlayerPed.Handle, vehicle, false)))
            {
                int timer = _helicopterRespawnTime;
                while (timer > 0)
                {
                    SetCenterText($"RESPAWN IN {Math.Round(timer / 1000.0f)}", Color.FromArgb(255, 255, 255), 200, 100, 0);
                    timer -= 100;
                    await Delay(100);
                    
                    if (IsPedVehicleBuzzard()) return; // abort spawn countdown if player gets back to helicopter
                    if (!CanRespawnAsHelicopter()) return;
                }

                if (!CanRespawnAsHelicopter()) return;

                Player target = GetChasedPlayer();
                
                Vector3 pos = GetEntityCoords(target.Character.Handle, true);
                float heading = GetEntityHeading(target.Character.Handle);
                
                BecomeChaserHelicopter(pos, heading);
            }
        }

        private async Task OnHelicopterBombsTick()
        {
            var bombs = _helicopterBombs.ToList();
            foreach (var bomb in bombs)
            {
                bomb.ApplyForce(new Vector3(0, 0, -1.0f));
                
                var coords = GetEntityCoords(bomb.Handle, true);
                var groundZ = 0.0f;
                GetGroundZFor_3dCoord(coords.X, coords.Y, coords.Z - 2, ref groundZ, false);

                if (groundZ <= 0)
                {
                    AddExplosion(coords.X, coords.Y, coords.Z, (int) ExplosionType.Tanker, 1.0f, true, false, 1.0f);

                    _helicopterBombs.Remove(bomb);
                    var handle = bomb.Handle;
                    DeleteEntity(ref handle);
                }
            }
        }
        
        private async Task OnHelicopterAttackTick()
        {
            if (!IsPedVehicleBuzzard()) return;

            if (IsControlPressed(25, 114))
            {
                int vehicle = GetVehiclePedIsIn(Game.PlayerPed.Handle, false);

                int numBarrels = 3;
                while (numBarrels > 0)
                {
                    numBarrels--;
                    
                    Vector3 pos = GetEntityCoords(vehicle, true);
                    Vector3 vel = GetEntityVelocity(vehicle);

                    // apply partial velocity to the trajectory of the barrel
                    // vel.Z = 0;
                    vel *= 3.0f;
                
                    Vector3 addVel = new Vector3(vel.X, vel.Y, vel.Z < 0 ? vel.Z : 0) * 0.05f;
                    Prop prop = await World.CreateProp("prop_rad_waste_barrel_01", new Vector3(pos.X, pos.Y, pos.Z + addVel.Z - 2.0f), true, false);
                    prop.ApplyForce(new Vector3(0, 0, -15.0f) + vel);
                    _helicopterBombs.Add(prop);
                
                    PlaySoundFrontend(-1, "Dropped", "HUD_FRONTEND_MP_COLLECTABLE_SOUNDS", false);

                    Screen.ShowSubtitle($"~r~Reloading...~s~", _helicopterAttachCooldownTime);

                    await Delay(300);
                }

                await Delay(_helicopterAttachCooldownTime);
                Screen.ShowSubtitle($"~g~RELOADED~s~", 1000);
                
                PlaySoundFrontend(-1, "CHECKPOINT_PERFECT", "HUD_MINI_GAME_SOUNDSET", false);
            }
        }

        private Scores.ScoresData _testScores;
        
        private void SetTestScores()
        {
            _testScores = new Scores.ScoresData();

            _testScores.bestTurnBy = "1";
            _testScores.bestTurnEscaped = true;
            _testScores.bestTurnTime = 12000;

            _testScores.data = new List<Scores.ScoreData>();
            
            var p = new Scores.ScoreData();
            p.totalScore = 20;
            p.playerHandle = "5";
            p.turnsEscaped = 5;
            p.currentRoundTime = -1;
            p.currentRoundEscaped = false;
            
            _testScores.data.Add(p);

            p = new Scores.ScoreData();
            p.totalScore = 100;
            p.playerHandle = "4";
            p.turnsEscaped = 3;
            p.currentRoundTime = 250000;
            p.currentRoundEscaped = false;
            
            _testScores.data.Add(p);
            
            p = new Scores.ScoreData();
            p.totalScore = 150;
            p.playerHandle = "3";
            p.turnsEscaped = 1;
            p.currentRoundTime = 50400;
            p.currentRoundEscaped = false;
            
            _testScores.data.Add(p);
            
            p = new Scores.ScoreData();
            p.totalScore = 300;
            p.playerHandle = "2";
            p.turnsEscaped = 2;
            p.currentRoundTime = 55000;
            p.currentRoundEscaped = true;
            
            _testScores.data.Add(p);
            
            p = new Scores.ScoreData();
            p.totalScore = 10;
            p.playerHandle = "1";
            p.turnsEscaped = 5;
            p.currentRoundTime = 25000;
            p.currentRoundEscaped = true;
            
            _testScores.data.Add(p);
        }

        private void DrawScoreTextRow(string text, ref float yPos)
        {
            SetTextFont(4);
            SetTextScale(1, 0.5f);
            SetTextColour(255, 255, 255, 200);
            SetTextDropShadow();
            SetTextProportional(true);
            SetTextEntry("STRING");
            SetTextOutline();
            SetTextRightJustify(true);
            AddTextComponentString(text);
            DrawText(0.9f, yPos);

            yPos += 0.03f;
        }

        private async Task OnDrawScores()
        {
            // var scoresData = _testScores;
            var scoresData = _scores.scoresData;
            
            if (scoresData == null || scoresData.data.Count <= 0) return;

            PlayerList playerList = new PlayerList();

            float yPos = 0.022f;
            DrawScoreTextRow($"ROUNDS PLAYED: {scoresData.roundsPlayed}", ref yPos);

            if (scoresData.bestTurnTime > 0)
            {
                foreach (var player in playerList)
                {
                    if (GetPlayerServerId(player.Handle) != int.Parse(scoresData.bestTurnBy)) continue;
                
                    string escaped = scoresData.bestTurnEscaped
                        ? "~g~ESCAPED~s~"
                        : "~r~CAUGHT~s~";

                    string time = TimeToString(scoresData.bestTurnTime);
                
                    DrawScoreTextRow($"BEST TURN: ~b~{player.Name}~s~ {escaped} ~y~{time}~s~", ref yPos);
                }
            }
            
            yPos += 0.015f;
            DrawScoreTextRow("SCORES", ref yPos);
            
            var totalScoresOrdered = scoresData.data
                .OrderBy(d => -d.totalScore)
                .ToList();

            foreach (var scoreData in totalScoresOrdered)
            {
                foreach (var player in playerList)
                {
                    if (GetPlayerServerId(player.Handle) != int.Parse(scoreData.playerHandle)) continue;
                    
                    DrawScoreTextRow($"~b~{player.Name}~s~ ~y~{scoreData.totalScore}~s~", ref yPos);
                }
            }

            yPos += 0.1f;
            DrawScoreTextRow($"CURRENT ROUND", ref yPos);
            
            var escapedScoreData = scoresData.data
                .Where(d => d.currentRoundEscaped)
                .OrderBy(d => d.currentRoundTime)
                .ToList();
            
            var caughtScoreData = scoresData.data
                .Where(d => !d.currentRoundEscaped)
                .OrderBy(d => -d.currentRoundTime)
                .ToList();
            
            var currentRoundScoresOrdered = escapedScoreData;
            currentRoundScoresOrdered.AddRange(caughtScoreData);
            
            foreach (var scoreData in currentRoundScoresOrdered)
            {
                foreach (var player in playerList)
                {
                    if (GetPlayerServerId(player.Handle) != int.Parse(scoreData.playerHandle)) continue;
                    
                    if (scoreData.currentRoundTime <= 0) continue;
                    
                    string escaped = scoreData.currentRoundEscaped
                        ? "~g~ESCAPED~s~"
                        : "~r~CAUGHT~s~";

                    string time = TimeToString(scoreData.currentRoundTime);
                    
                    DrawScoreTextRow($"~b~{player.Name}~s~ {escaped} ~y~{time}~s~", ref yPos);
                }
            }
        }
        
        private async void OnClientLog(int source, List<object> args, string raw)
        {
            // int vehicle = GetVehiclePedIsIn(PlayerPedId(), true);
        }
        
        private async void OnClientLog2()
        { 
            // int vehicle = GetVehiclePedIsIn(PlayerPedId(), true);
        }

        private void OnServerCommandGMHeatChasedValues(int chasedPlayer, int heatDistance, int heatPercent, int heatAppliedCurrently, int chaseTime)
        {
            _chasedPlayerHandle = chasedPlayer;
            _heatDistance = heatDistance;
            _heatPercent = heatPercent / 100.0f;
            _heatAppliedCurrently = heatAppliedCurrently / 100.0f;
            _chaseTime = chaseTime;
        }

        private void OnServerCommandGMHeatCaughtChased()
        {
            SetCenterText("CAUGHT", Color.FromArgb(255, 0, 0), 4000, 100, 1);
            PlaySoundFrontend(-1, "ScreenFlash", "WastedSounds", false);
        }

        private void OnServerCommandGMHeatCaughtChasers()
        {
            SetCenterText("CAUGHT", Color.FromArgb(0, 255, 0), 4000, 100, 1);
            PlaySoundFrontend(-1, "SHOOTING_RANGE_ROUND_OVER", "HUD_AWARDS", false);
        }

        private void OnServerCommandGMHeatEscapedChased()
        {
            SetCenterText("ESCAPED", Color.FromArgb(0, 255, 0), 4000, 100, 1);
            PlaySoundFrontend(-1, "SHOOTING_RANGE_ROUND_OVER", "HUD_AWARDS", false);
        }

        private void OnServerCommandGMHeatEscapedChasers()
        {
            SetCenterText("ESCAPED", Color.FromArgb(255, 0, 0), 4000, 100, 1);
            PlaySoundFrontend(-1, "ScreenFlash", "WastedSounds", false);
        }
        
        private void OnClientResourceStop(string resourceName)
        {
            if(GetCurrentResourceName() != resourceName) return;
            
            OnEnableControl();
        }

        private string TimeToString(int time)
        {
            int seconds = (int)Math.Floor((float)(time % 60000) / 1000);
            int minutes = (int)Math.Floor((float)time / 60000);

            string secondsStr = seconds < 10 ? $"0{seconds}" : seconds.ToString();
            string minutesStr = minutes < 10 ? $"0{minutes}" : minutes.ToString();

            return $"{minutesStr}:{secondsStr}";
        }

        private void DrawChaseTime()
        {
            if (_chaseTime < 0) return;

            SetTextFont(4);
            SetTextScale(1, 1);
            SetTextColour(255, 255, 255, 200);
            SetTextDropShadow();
            SetTextProportional(true);
            SetTextEntry("STRING");
            AddTextComponentString(TimeToString(_chaseTime));
            SetTextCentre(true);
            DrawText(0.5f, 0.06f);
        }

        private void DrawChaseHeatBar()
        {
            if (_heatPercent <= 0 && _heatAppliedCurrently <= 0) return;
            
            float heatAppliedPercent = _heatAppliedCurrently;

            Color color = heatAppliedPercent > 0
                ? Utils.LerpColor(_heatColorMin, _heatColorMax, heatAppliedPercent)
                : _fadeColor;

            float minScaleMultiplier = 1.5f;
            float maxScaleMultiplier = 2.0f;
            float scale = Utils.Lerp(minScaleMultiplier, maxScaleMultiplier, heatAppliedPercent);
            
            int minAlpha = 120;
            int maxAlpha = 150;
            int alpha = (int)Math.Round(Utils.Lerp(minAlpha, maxAlpha, heatAppliedPercent));

            float x = 0.5f;
            float y = 0.13f;
            float maxWidth = 0.1f;
            float heatWidth = maxWidth * _heatPercent;
            DrawRect(x, y, 0.1f * scale, 0.01f * scale, 0, 0, 0, 75);
            DrawRect(x, y, heatWidth *  scale, 0.01f * scale, color.R, color.G, color.B, alpha);
        }

        private long _prevTickTime;
        private long _deltaTime;

        private async Task OnTick()
        {
            _deltaTime = _prevTickTime > 0
                ? GetGameTimer() - _prevTickTime
                : 0;

            _prevTickTime = GetGameTimer();

            UpdateHUD();
        }

        private void OnServerCommandSetDifficultyLevel(int level)
        {
            _difficultyLevel = level;
            
            switch (_difficultyLevel)
            {
                case 0:
                    _wantedLevel = 0;
                    _chasedWeapon = (uint)GetHashKey("WEAPON_KNIFE");
                    _chaserWeapon = (uint)GetHashKey("WEAPON_BAT");
                    _difficultyDescription = "";
                    break;
                case 1:
                    _wantedLevel = 0;
                    _chasedWeapon = (uint)GetHashKey("WEAPON_KNIFE");
                    _chaserWeapon = (uint)GetHashKey("WEAPON_SAWNOFFSHOTGUN");
                    _difficultyDescription = "SAWED-OFF SHOTGUNS";
                    break;
                case 2:
                    _wantedLevel = 0;
                    _chasedWeapon = (uint)GetHashKey("WEAPON_KNIFE");
                    _chaserWeapon = (uint)GetHashKey("WEAPON_PISTOL");
                    _difficultyDescription = "PISTOLS";
                    break;
                case 3:
                    _wantedLevel = 3;
                    _chasedWeapon = (uint)GetHashKey("WEAPON_KNIFE");
                    _chaserWeapon = (uint)GetHashKey("WEAPON_MICROSMG");
                    _difficultyDescription = "MICRO SMG'S, WANTED LEVEL 3";
                    break;
                case 4:
                    _wantedLevel = 4;
                    _chasedWeapon = (uint)GetHashKey("WEAPON_KNIFE");
                    _chaserWeapon = (uint)GetHashKey("WEAPON_MICROSMG");
                    _difficultyDescription = "MICRO SMG'S, WANTED LEVEL 4";
                    break;
                case 5:
                    _wantedLevel = 5;
                    _chasedWeapon = (uint)GetHashKey("WEAPON_KNIFE");
                    _chaserWeapon = (uint)GetHashKey("WEAPON_MICROSMG");
                    _difficultyDescription = "MICRO SMG'S, WANTED LEVEL 5";
                    break;
            }

            if (_difficultyLevel > 0)
            {
                SetCenterText($"LEVEL {_difficultyLevel}", Color.FromArgb(255, 0, 0), 2500, 100, 4);
            }
            
        }

        private uint _chasedWeapon;
        private uint _chaserWeapon;

        private async Task OnDifficultyLevelTick()
        {
            var chased = GetChasedPlayer();
            
            if (chased == null) return;

            // Set / check ped weapon on every tick to make sure user can't switch weapon even when one found.
            var playerList = Players;
            foreach (var player in playerList)
            {
                uint currentWeaponHash = 0;
                GetCurrentPedWeapon(Game.PlayerPed.Handle, ref currentWeaponHash, false);

                uint useWeaponHash = player == chased ? _chasedWeapon : _chaserWeapon;
                if (currentWeaponHash != useWeaponHash)
                {
                    GiveWeaponToPed(Game.PlayerPed.Handle, useWeaponHash, int.MaxValue, false, true);
                }
            }

            int wantedLevel = Game.Player == chased ? _wantedLevel : 0;
            
            if (GetPlayerWantedLevel(Game.Player.Handle) != wantedLevel)
            {
                SetPlayerWantedLevelNoDrop(Game.Player.Handle, _wantedLevel, false);
                SetPlayerWantedLevelNow(Game.Player.Handle, false);
            }
        }
        
        private int _minSpawnDistance = 300;
        private int _spawnStayStillTime = 5000;
        private int _spawnStayStillTimer;
        private int _maxSpawnSpeed = 2;
        private int _spawnInitMinSpeed = 10;

        private bool IsRespawnAllowed()
        {
            var chasedPlayer = GetChasedPlayer();
            return _heatDistance > 0.0f && chasedPlayer != null && chasedPlayer != Game.Player && !IsPedVehicleBuzzard();
        }
        
        private async Task OnRespawnTick()
        {
            if (!IsRespawnAllowed()) return;

            Player chasedPlayer = GetChasedPlayer();
            float speed = GetEntitySpeed(Game.PlayerPed.Handle);

            bool inSpawnDistance = Vector3.Distance(Game.PlayerPed.Position, chasedPlayer.Character.Position) > _minSpawnDistance;
            if (!inSpawnDistance) return;

            int currentVehicle = GetVehiclePedIsIn(Game.PlayerPed.Handle, false);
            if (currentVehicle <= 0)
            {
                SetCenterText("FIND CAR TO SPAWN", Color.FromArgb(255, 255, 255), 1000, 100, 0);
                return;
            }

            SetCenterText("STAY STILL TO SPAWN", Color.FromArgb(255, 255, 255), 1000, 100, 0);

            bool inSpawnSpeed = speed < _maxSpawnSpeed;
            if (!inSpawnSpeed) return;
            
            _spawnStayStillTimer = _spawnStayStillTime;
                    
            while (_spawnStayStillTimer > 0)
            {
                inSpawnSpeed = speed < _maxSpawnSpeed;
                currentVehicle = GetVehiclePedIsIn(Game.PlayerPed.Handle, false);

                if (!inSpawnSpeed) return;
                if (currentVehicle <= 0) return;

                speed = GetEntitySpeed(Game.PlayerPed.Handle);
                        
                SetCenterText($"SPAWN IN {Math.Round(_spawnStayStillTimer / 1000.0f)}", Color.FromArgb(255, 255, 255), 1100, 100, 0);
                        
                _spawnStayStillTimer -= 1000;
                await Delay(1000);
            }
            
            currentVehicle = GetVehiclePedIsIn(Game.PlayerPed.Handle, false);

            if (currentVehicle <= 0)
            {
                Screen.ShowNotification("Vehicle required to spawn...");
                return;
            }
            
            Player bestSpawnPlayer = null;
            int bestSpawnVehicle = 0;
            float bestSpawnDistance = int.MaxValue;

            var chased = GetChasedPlayer();
            // var chased = Game.Player;
            var playerList = Players;
            // var players = new PlayerList().ToList();
            foreach (Player player in playerList)
            {
                if (chased == player) continue;
                if (IsVehicleBuzzard(GetVehiclePedIsIn(player.Character.Handle, true))) continue;

                var vehicle = GetVehiclePedIsIn(player.Character.Handle, false);
                
                // calc distance in 2d plane, so that elevation differences or helicopter fly height does not affect distance.
                float dist = Vector3.Distance(
                    new Vector3(chased.Character.Position.X, chased.Character.Position.Y, 0), 
                    new Vector3(player.Character.Position.X, player.Character.Position.Y, 0)
                );
                
                if (dist > 200) continue; // 200 is escape distance, so lets not allow player to spawn target further than that

                if (bestSpawnPlayer == null || dist < bestSpawnDistance || (bestSpawnVehicle <= 0 && vehicle > 0))
                {
                    bestSpawnPlayer = player;
                    bestSpawnDistance = dist;
                    bestSpawnVehicle = vehicle;
                }
            }

            if (bestSpawnPlayer == null || bestSpawnPlayer.Character == null)
            {
                bestSpawnPlayer = chased;
            }
            
            if (!IsRespawnAllowed()) // check once more if ok to spawn
            {
                return;
            }

            int spawnTarget = bestSpawnPlayer.Character.Handle;

            Vector3 targetCoords = GetEntityCoords(spawnTarget, true);
            float targetHeading = GetEntityHeading(spawnTarget);
            Vector3 targetVelocityNormalized = GetEntityVelocity(spawnTarget);
            targetVelocityNormalized.Normalize();
            float targetSpeed = GetEntitySpeed(spawnTarget);

            Vector3 initVelocity = targetVelocityNormalized * Math.Max(targetSpeed * 0.5f, _spawnInitMinSpeed);
            
            // Check if the spawn target is on air and in case that's true we want to spawn under it (helicopter etc.)
            float groundZ = -1;
            GetGroundZFor_3dCoord(targetCoords.X, targetCoords.Y, targetCoords.Z, ref groundZ, false);

            float zOffset = targetCoords.Z - groundZ > 5.0f ? -5 : 5;

            if (bestSpawnPlayer == chased)
            {
                await Delay(500);
            }

            SetEntityCoords(currentVehicle, targetCoords.X, targetCoords.Y, targetCoords.Z + zOffset, false, false, false, false);
            SetEntityHeading(currentVehicle, targetHeading);
            SetEntityVelocity(currentVehicle, initVelocity.X, initVelocity.Y, 0);
        }

        private Dictionary<int, int> _playerBlips = new Dictionary<int, int>();

        private async Task OnUpdateNewPlayerBlipsTick()
        {
            await Delay(1000);
            
            PlayerList playerList = new PlayerList();
            foreach (var player in playerList)
            {
                int handle = player.Character.Handle;
                if (!_playerBlips.ContainsKey(player.Character.Handle))
                {
                    int blip = AddBlipForEntity(handle);
                    SetBlipColour(blip, 5);
                    SetBlipScale(blip, 1.1f);
                    _playerBlips[handle] = blip;
                }
            }
        }

        private void SetCenterText(string text, Color color, int time = 4000, int alpha = 100, int style = 0)
        {
            _centerText = text;
            _centerTextAlpha = alpha;
            _centerTextColor = color;
            _centerTextTimer = time;
            _centerTextStyle = style;
        }

        private void DrawCenterText()
        {
            if (_centerTextTimer <= 0.0f) return;

            _centerTextTimer -= _deltaTime;
            
            if (_centerText == "") return;
            
            if (_centerTextStyle == 1)
            {
                SetTextFont(1);
                SetTextScale(1, 5);
            }
            else
            {
                SetTextFont(4);
                SetTextScale(1, 2);
            }

            SetTextColour(_centerTextColor.R, _centerTextColor.G, _centerTextColor.B, _centerTextAlpha);
            SetTextDropShadow();
            SetTextProportional(true);
            SetTextEntry("STRING");
            AddTextComponentString(_centerText);
            SetTextCentre(true);
            DrawText(0.5f, 0.15f);
        }

        private void DrawDifficulty()
        {
            if (_difficultyLevel < 0) return;
            
            SetTextFont(4);
            SetTextScale(1, 0.75f);
            SetTextColour(255, 255, 255, 150);
            SetTextDropShadow();
            SetTextProportional(true);
            SetTextEntry("STRING");
            AddTextComponentString($"LEVEL {_difficultyLevel + 1}");
            SetTextCentre(true);
            DrawText(0.5f, 0.02f);
            
            SetTextFont(4);
            SetTextScale(1, 0.3f);
            SetTextColour(255, 0, 0, 255);
            SetTextProportional(true);
            SetTextEntry("STRING");
            AddTextComponentString(_difficultyDescription.ToUpper());
            SetTextCentre(true);
            DrawText(0.5f, 0.007f);
        }

        private void UpdateHUD()
        {
            DrawChaseTime();
            DrawChaseHeatBar();
            DrawCenterText();
            DrawDifficulty();
        }

        private void OnServerCommandSubtitle(string notification, int duration = 2500)
        {
            Screen.ShowSubtitle(notification, duration);
        }

        private void OnServerCommandNotification(string notification)
        {
            Screen.ShowNotification($"{notification}!");
        }

        private void OnServerCommandCenterText(string text, int time, int alpha, int style)
        {
            SetCenterText(text, Color.FromArgb(0, 255, 255), time, alpha, style);
            PlaySound(-1, "CANCEL", "HUD_MINI_GAME_SOUNDSET", false, 0, false);
        }
        
        private void OnInfo()
        {
            Screen.ShowNotification($"Hello and whalecum {Game.Player.Name}!");
        }
        
        private void OnResetCar()
        {
            ClientActions.FixCar();
        }
        
        private void OnDisableControl()
        {
            SetPlayerControl(Game.Player.Handle, false, 256);
        }
        
        private void OnEnableControl()
        {
            SetPlayerControl(Game.Player.Handle, true, 0);
        }

        private async void OnServerCommandSpawnCar(string name)
        {
            int vehicle = GetVehiclePedIsIn(Game.PlayerPed.Handle, false);
            if (vehicle != 0)
            {
                DeleteEntity(ref vehicle);
            }

            // pick random vehicle from preferred categories
            if (name == "random")
            {
                name = _vehicles.GetRandomStartCategory();
            }
            
            // Check if name is actually category and randomize vehicle from that category
            if (_vehicles.IsAllowedVehicleCategory(name))
            {
                string category = name;
                Vehicles.VehicleData vehicleData = _vehicles.GetRandomVehicle(category);
                await ClientActions.SpawnCar(vehicleData.name);
                return;
            }

            await ClientActions.SpawnCar(name);
        }
        
        private void OnServerCommandSetSpeedLimiter(int speed)
        {
            int vehicle = GetVehiclePedIsIn(Game.PlayerPed.Handle, false);
            if (vehicle != 0)
            {
                // SetEntityMaxSpeed(vehicle, speed);
            }
        }

        private async void OnServerCommandGMRoundStarting()
        {
            SetPlayerWantedLevel(Game.Player.Handle, 0, false);
            SetEntityHealth(Game.PlayerPed.Handle, GetEntityMaxHealth(Game.PlayerPed.Handle));

            _wantedLevel = 0;
        }

        private async Task OnUpdatePlayerBlips()
        {
            PlayerList playerList = new PlayerList(); 
            foreach (var player in playerList)
            {
                if (_playerBlips.ContainsKey(player.Character.Handle))
                {
                    int blip = _playerBlips[player.Character.Handle];
                    
                    // display helicopter always
                    // if (IsVehicleBuzzard(GetVehiclePedIsIn(player.Character.Handle, false)))
                    // {
                    //     SetBlipDisplay(blip, 2); // display in minimap and map
                    //     continue;
                    // }
                    
                    SetBlipShowCone(blip, false);
                    
                    if (GetPlayerServerId(player.Handle) == _chasedPlayerHandle) // chased
                    {
                        if (ImChased())
                        {
                            SetBlipDisplay(blip, 0); // hide
                        }
                        else
                        {
                            SetBlipDisplay(blip, 2); // display in minimap and map
                            SetBlipColour(blip, 5);
                        }
                    }
                    else // chasers
                    {
                        if (ImChased())
                        {
                            SetBlipDisplay(blip, 0); // hide
                        }
                        else
                        {
                            SetBlipDisplay(blip, 2); // display in minimap and map
                            SetBlipColour(blip, 18);
                        }
                    }
                }
            }
        }

        private Player GetChasedPlayer()
        {
            PlayerList playerList = new PlayerList();
            foreach (var player in playerList)
            {
                if (GetPlayerServerId(player.Handle) != _chasedPlayerHandle) continue;
                return player;
            }

            return null;
        }

        private List<Player> GetChasingPlayers()
        {
            List<Player> players = new List<Player>();
            PlayerList playerList = new PlayerList();
            foreach (var player in playerList)
            {
                if (GetPlayerServerId(player.Handle) == _chasedPlayerHandle) continue;
                players.Add(player);
            }

            return players;
        }

        private int GetChasedVehicle()
        {
            Player chasedPlayer = GetChasedPlayer();
            int chasedVehicle = GetVehiclePedIsIn(chasedPlayer.Character.Handle, false);
            return chasedVehicle;
        }

        private async Task<Vehicle> SpawnVehicle(int vehicle)
        {
            return await World.CreateVehicle(vehicle, Game.PlayerPed.Position, Game.PlayerPed.Heading);
        }

        private async Task<int> ResolveRoundStartVehicle()
        {
            int vehicleToUse = 0;
            
            int currentVehicle = GetVehiclePedIsIn(Game.Player.Character.Handle, true);

            if (IsPedVehicleBuzzard())
            {
                if (_preHelicopterVehicleHandle != 0)
                {
                    // 1) check if the pre helicopter vehicle exists and has HP,
                    // 2) spawn new vehicle by using the pre helicopter vehicle hash
                    int hp = GetEntityHealth(_preHelicopterVehicleHandle);
                    if (hp > 0)
                    {
                        vehicleToUse = _preHelicopterVehicleHandle;
                    }
                    else if (_preHelicopterVehicleModel != 0)
                    {
                        Vehicle spawnedVehicle = await World.CreateVehicle(_preHelicopterVehicleModel, Game.PlayerPed.Position, Game.PlayerPed.Heading);
                        vehicleToUse = spawnedVehicle.Handle;
                    }
                }
                
                DeleteEntity(ref currentVehicle);
            }
            else
            {
                vehicleToUse = currentVehicle;
            }

            bool vehicleAllowed = _vehicles.IsAllowedVehicle(vehicleToUse);
            if (vehicleToUse == 0 || !vehicleAllowed)
            {
                if (!vehicleAllowed && vehicleToUse != 0)
                {
                    DeleteEntity(ref vehicleToUse);
                    Screen.ShowNotification("Vehicle not allowed");
                }
                
                Vehicle randomVehicle = await World.CreateVehicle("emperor", Game.PlayerPed.Position, Game.PlayerPed.Heading);
                vehicleToUse = randomVehicle.Handle;
            }

            if (_helicopter != null)
            {
                int helicopterHandle = _helicopter.Handle;
                DeleteEntity(ref helicopterHandle);
                _helicopter = null;
            }

            return vehicleToUse;
        }

        private async void OnServerCommandGMSpawn(Vector3 spawnPoint, int spawnAngle, bool cloneChasedCar)
        {
            int vehicleToUse = 0;
            
            // try to replace current car with chased players car if clone enabled
            if (cloneChasedCar)
            {
                int currentVehicle = GetVehiclePedIsIn(Game.PlayerPed.Handle, true);
                int chasedVehicle = GetChasedVehicle();
                
                // clone same vehicle only if there is something to clone and if the vehicle model differs
                if (chasedVehicle != 0 && GetEntityModel(chasedVehicle) != GetEntityModel(currentVehicle))
                {
                    // cleanup current car as we now have new car
                    if (currentVehicle != 0)
                    {
                        int handle = currentVehicle;
                        DeleteEntity(ref handle);
                    }
                    
                    Vehicle veh = await SpawnVehicle(GetEntityModel(chasedVehicle));
                    vehicleToUse = veh.Handle;
                }
            }

            if (!cloneChasedCar || vehicleToUse == 0)
            {
                vehicleToUse = await ResolveRoundStartVehicle();
            }

            SetPedIntoVehicle(Game.PlayerPed.Handle, vehicleToUse, -1);

            int entityToTeleport = vehicleToUse != 0 ? vehicleToUse : Game.PlayerPed.Handle;
            SetEntityCoords(entityToTeleport, spawnPoint.X, spawnPoint.Y, spawnPoint.Z, false, false, false, true);
            SetEntityHeading(entityToTeleport, spawnAngle);

            ClearAreaOfVehicles(spawnPoint.X, spawnPoint.Y, spawnPoint.Z, 100, false, false, false, false, false);
            ClearAreaOfEverything(spawnPoint.X, spawnPoint.Y, spawnPoint.Z, 100, false, false, false, false);
        }

        private async void OnServerCommandBecomeHelicopter(Vector3 atPosition, int heading)
        {
            BecomeChaserHelicopter(atPosition, heading);
        }

        private bool IsPedVehicleBuzzard(bool lastVehicle = false)
        {
            return IsVehicleBuzzard(GetVehiclePedIsIn(Game.PlayerPed.Handle, lastVehicle));
        }
        
        private bool IsVehicleBuzzard(int vehicle)
        {
            return GetEntityModel(vehicle) == GetHashKey("buzzard2");
        }

        private async void BecomeChaserHelicopter(Vector3 atPosition, float heading)
        {
            int vehicle = GetVehiclePedIsIn(Game.PlayerPed.Handle, true);

            if (!IsPedVehicleBuzzard())
            {
                _preHelicopterVehicleHandle = vehicle;
                _preHelicopterVehicleModel = GetEntityModel(vehicle);
            }

            _helicopter = await World.CreateVehicle("buzzard2", Game.PlayerPed.Position, heading);
            Game.PlayerPed.SetIntoVehicle(_helicopter, VehicleSeat.Driver);
            
            float groundZ = 0;
            GetGroundZFor_3dCoord(atPosition.X, atPosition.Y, atPosition.Z + 1000, ref groundZ, false);

            float offset = 100;
            groundZ += offset;
            SetEntityCoords(_helicopter.Handle, atPosition.X, atPosition.Y, atPosition.Z + groundZ, false, false, false, true);

            if (_playerBlips.ContainsKey(_chasedPlayerHandle))
            {
                // Hide blip of chased player when using helicopter
                SetBlipDisplay(_playerBlips[_chasedPlayerHandle], 0);
            }
        }

        private async void  OnCommandSpawnCar(int source, List<object> args, string raw)
        {
            // account for the argument not being passed
            var model = "adder";
            if (args.Count > 0)
            {
                model = args[0].ToString();
            }

            int currentVehicle = GetVehiclePedIsIn(Game.PlayerPed.Handle, false);

            if (!await ClientActions.SpawnCar(model))
            {
                TriggerEvent("chat:addMessage", new 
                {
                    color = new[] { 255, 0, 0 },
                    args = new[] { "[CarSpawner]", $"It might have been a good thing that you tried to spawn a {model}. Who even wants their spawning to actually ^*succeed?" }
                });
            }
            else
            {
                DeleteEntity(ref currentVehicle);
            }

            // tell the player
            TriggerEvent("chat:addMessage", new 
            {
                color = new[] {255, 0, 0},
                args = new[] {"[CarSpawner]", $"Enjoy your new ^*{model}!"}
            });
        }

        private void OnServerCommandUpdateScores(string jsonString)
        {
            Scores.ScoresData scoresData = _scores.ScoresJSONTOObject(jsonString);
            _scores.SetScores(scoresData);
        }

        private void OnServerCommandKill()
        {
            SetEntityHealth(Game.PlayerPed.Handle, 0);
        }

        private void OnCommandFixCar(int source, List<object> args, string raw)
        {
            ClientActions.FixCar();
        }

        private bool _passive;

        private void OnCommandPassiveMode(int source, List<object> args, string raw)
        {
            Screen.ShowNotification(_passive ? "Passive mode: OFF" : "Passive mode: ON");
            _passive = !_passive;
            NetworkSetPlayerIsPassive(_passive);
        }

        private void OnClearArea()
        {
            Vector3 point = Game.PlayerPed.Position;
            ClearAreaOfVehicles(point.X, point.Y, point.Z, 100, false, false, false, false, false);
            ClearAreaOfEverything(point.X, point.Y, point.Z, 100, false, false, false, false);
        }

        private void OnCommandSuicide()
        {
            SetEntityHealth(Game.Player.Character.Handle, 0);
        }

        private bool ImChased()
        {
            return _chasedPlayerHandle == GetPlayerServerId(Game.Player.Handle);
        }
    }
}