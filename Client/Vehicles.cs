﻿using System;
using System.Collections.Generic;
using CitizenFX.Core;
using Newtonsoft.Json;
using static CitizenFX.Core.Native.API;

namespace Client
{
    public class Vehicles
    {
        private List<VehicleData> _vehicles;
        
        private List<string> _allowedVehicleCategories = new List<string>()
        {
            "Vans",
            // "Super",
            "Sports Classics",
            // "Sports",
            "Sedans",
            "SUVs",
            "Off-Road",
            "Muscle",
            "Motorcycles",
            "Cycles",
            "Coupes",
            "Compacts",
        };
        
        private List<string> _defaultStartCategories = new List<string>()
        {
            "Sports Classics",
            "Sedans",
            "SUVs",
            "Off-Road",
            "Muscle",
            "Motorcycles",
            "Coupes",
            "Compacts",
        };
        
        private List<string> _bannedVehicleCategories = new List<string>()
        {
            "Service", // Check bounding box
            "Utility", // Check bounding box
            "Military", // Check bounding box
            "Emergency", // Check bounding box - no Polmav, Predator
            "Commercials", // Check bounding box
            "Trailer",
            "Planes",
            "Industrial",
            "Helicopters",
            "Boats",
        };

        public Vehicles() {}
        
        public class VehicleData
        {
            public string name;
            public string id;
            public string category;
            public string dlc;

            public new string ToString()
            {
                return $"{name}, {category}, {dlc} [{id}]";
            }
        }
        
        public void LoadVehiclesData()
        {
            string json = LoadResourceFile(GetCurrentResourceName(), "vehicles.json");
            _vehicles = JsonConvert.DeserializeObject<List<VehicleData>>(json);
        }

        public VehicleData GetRandomVehicle(string category = "Super")
        {
            List<VehicleData> vehicles = _vehicles.FindAll(vehicle => string.Equals(vehicle.category, category, StringComparison.CurrentCultureIgnoreCase));
            Random rand = new Random();
            int random = rand.Next(0, vehicles.Count - 1);

            return vehicles[random];
        }

        public VehicleData GetVehicleData(int vehicle)
        {
            int hash = GetEntityModel(vehicle);
            return _vehicles.Find(vehicleData => hash.ToString() == vehicleData.id);
        }

        public string GetVehicleCategory(int vehicle)
        {
            var data = GetVehicleData(vehicle);
            return data != null ? data.category : "";
        }

        public bool IsAllowedVehicle(int vehicle)
        {
            var data = GetVehicleData(vehicle);
            var allowed = data != null && _allowedVehicleCategories.Contains(data.category);
            
            return allowed;
        }
        
        public bool IsAllowedVehicleCategory(string category)
        {
            return _allowedVehicleCategories.FindIndex(cat => string.Equals(cat, category, StringComparison.CurrentCultureIgnoreCase)) != -1;
        }

        public string GetRandomStartCategory()
        {
            Random rand = new Random();
            int random = rand.Next(0, _defaultStartCategories.Count - 1);

            return _defaultStartCategories[random];
        }
    }
}