﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.UI;
using static CitizenFX.Core.Native.API;

namespace Client
{
    public class ClientActions
    {
        public ClientActions() {}

        public static bool FixCar()
        {
            if (IsPedInAnyVehicle(PlayerPedId(), true))
            {
                int vehicle = GetVehiclePedIsUsing(PlayerPedId());
                SetVehicleEngineHealth(vehicle, 1000);
                // SetVehicleEngineOn(vehicle, true, true, false);
                SetVehicleFixed(vehicle);
                SetVehicleDirtLevel(vehicle, 0);

                return true;
            }

            return false;
        }

        public static async Task<bool> SpawnCar(string model)
        {
            // check if the model actually exists
            // assumes the directive `using static CitizenFX.Core.Native.API;`
            var hash = (uint) GetHashKey(model);
            if (!IsModelInCdimage(hash) || !IsModelAVehicle(hash))
            {
                return false;
            }
            
            var vehicle = await World.CreateVehicle(model, Game.PlayerPed.Position, Game.PlayerPed.Heading);
            Game.PlayerPed.SetIntoVehicle(vehicle, VehicleSeat.Driver);
            
            return true;
        }
    }
}